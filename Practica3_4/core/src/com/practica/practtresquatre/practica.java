package com.practica.practtresquatre;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.utils.Timer;



public class practica extends ApplicationAdapter implements GestureDetector.GestureListener,InputProcessor {
	SpriteBatch batch;
	Texture img;
	int current_frame;
	String currentAtlasKey;
	TextureAtlas texAtlas;
	Sprite bird_sprite;
	float elapsetTime;
	float x,y;
	boolean move_x_n, move_x_p;
	TextureAtlas texAtlas2;
	Animation animation;
	InputMultiplexer im;
	GestureDetector gd;
	@Override
	public void create () {
		batch = new SpriteBatch();
		im = new InputMultiplexer();
		gd = new GestureDetector(this);
		move_x_p= false;move_x_n = false;
		im.addProcessor(gd);
		im.addProcessor(this);
		Gdx.input.setInputProcessor(im);
		current_frame = 1;
		texAtlas = new TextureAtlas(Gdx.files.internal("pack.atlas"));
		TextureAtlas.AtlasRegion region = texAtlas.findRegion("bird1");
		bird_sprite = new Sprite(region);
		bird_sprite.setPosition(120,100);
		bird_sprite.scale(3f);
		x = 0;
		y = 0;
		Timer.schedule(new Timer.Task() {
			@Override
			public void run() {
				current_frame++;
				if(current_frame > 6){
					current_frame = 1;
				}
				currentAtlasKey =  "bird"+String.format("%01d",current_frame);
				bird_sprite.setRegion(texAtlas.findRegion(currentAtlasKey));
			}
		},1,1/2.0f);

		texAtlas2 = new TextureAtlas(Gdx.files.internal("boy_anim.atlas"));
		animation = new Animation(1/15f, texAtlas2.getRegions());


	}


	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		if(move_x_p){x += 100*Gdx.graphics.getDeltaTime();}
		if(move_x_n){x -= 100*Gdx.graphics.getDeltaTime();}
		batch.begin();
		elapsetTime += Gdx.graphics.getDeltaTime();
		batch.draw((TextureRegion) animation.getKeyFrame(elapsetTime,true),x,y);
		bird_sprite.draw(batch);
		batch.end();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		texAtlas.dispose();
		texAtlas2.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		if(keycode == com.badlogic.gdx.Input.Keys.LEFT){
			move_x_n = true;
		}
		if(keycode == com.badlogic.gdx.Input.Keys.RIGHT){
			move_x_p = true;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if(keycode == com.badlogic.gdx.Input.Keys.LEFT){
			move_x_n = false;
		}
		if(keycode == com.badlogic.gdx.Input.Keys.RIGHT){
			move_x_p = false;
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if(screenX > 300){move_x_p = true;}
		if(screenX < 300){move_x_n = true;}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if(screenX > 300){move_x_p = false;}
		if(screenX < 300){move_x_n = false;}
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		return false;
	}

	@Override
	public void pinchStop() {

	}
}
