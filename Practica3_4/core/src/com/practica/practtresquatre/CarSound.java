package com.practica.practtresquatre;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.GLVersion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Timer;

/**
 * Created by joan.tuset on 08/02/2017.
 */

public class CarSound  extends ApplicationAdapter implements GestureDetector.GestureListener,InputProcessor {
    SpriteBatch batch;
    float elapsetTime;
    float velovity;
    boolean acc;
    InputMultiplexer im;
    GestureDetector gd;
    FileHandle soun1;
    FileHandle soun2;
    BitmapFont font;
    String text = "Your velocity: ";
    Sound Car_idle;
    Sound Car_run;
    long Id_idle;
    long Id_run;
    @Override
    public void create () {
        batch = new SpriteBatch();
        elapsetTime = 0;
        velovity = 0;
        im = new InputMultiplexer();
        gd = new GestureDetector(this);
        im.addProcessor(gd);
        im.addProcessor(this);
        Gdx.input.setInputProcessor(im);
        font = new BitmapFont();
        font.setColor(Color.BLACK);
        acc = false;
        Car_idle = Gdx.audio.newSound(new FileHandle("engine-idle.wav"));
        Car_run = Gdx.audio.newSound(new FileHandle("engine-running.wav"));
        Id_idle = Car_idle.play();
        Car_idle.setVolume(Id_idle,0.5f);
        Id_idle = Car_idle.loop();
        Id_run = Car_run.play();
        Id_run = Car_run.loop();
        Car_run.pause();
    }

    @Override
    public void render () {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        elapsetTime += Gdx.graphics.getDeltaTime();
        if(acc){
            if(velovity > 35){Car_idle.pause();}
            if(velovity <= 200){
                velovity += 10*Gdx.graphics.getDeltaTime();
                Car_run.setPitch(Id_run,velovity/100);
            }

        }
        else{
            if(velovity > 0){
                velovity -=10*Gdx.graphics.getDeltaTime();
                Car_run.setPitch(Id_run,velovity/100);
            }
        }
        if(velovity <= 30){
            Car_idle.resume(Id_idle);
        }

        batch.begin();
        font.draw(batch,velovity+" Km/h",200,200);
        batch.end();
    }

    @Override
    public void dispose () {
        batch.dispose();
        Car_idle.dispose();
        Car_run.dispose();

    }



    @Override
    public boolean keyDown(int keycode) {
        if(keycode == Input.Keys.SPACE){
            Car_run.resume(Id_run);

            acc = true;

        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if(keycode == Input.Keys.SPACE){
            Car_run.resume(Id_run);
            acc = false;
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public void pinchStop() {

    }
}
